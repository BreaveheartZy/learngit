package com.havefun.learngit;

import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.havefun.learngit.adapter.VisitorPageRVAdapter;

public class VisitorModeActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visitor_mode);

        RecyclerView recyclerView = findViewById(R.id.recycleView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new VisitorPageRVAdapter(this));

        findViewById(R.id.clBottomBar).setOnClickListener(this);
        findViewById(R.id.clTopBar).setOnClickListener(this);
        findViewById(R.id.clFilterBar).setOnClickListener(this);

        findViewById(R.id.ll1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TestDialog dialog = new TestDialog(VisitorModeActivity.this);
                dialog.show();
            }
        });

        TextView protocolText = findViewById(R.id.tvProtocol);
        protocolText.setHighlightColor(Color.WHITE);
        protocolText.setMovementMethod(LinkMovementMethod.getInstance());
        protocolText.setText(getAgreement());

    }

    private SpannableString getAgreement() {
        final String fileName1 = "《猎聘用户服务协议》";
        final String fileName2 = "《个人信息保护政策》";
        return SpannableHighLightAndClickUtils.getHighLightText("查看完整版" + fileName1 + "和" + fileName2 + "", new SpannableStringUtils.OnSpanClickListener() {
            @Override
            public void onSpanClick(String highLight) {
                if (fileName1.equals(highLight)) {
                    Toast.makeText(VisitorModeActivity.this, "协议1", Toast.LENGTH_SHORT).show();
                } else if (fileName2.equals(highLight)) {
                    Toast.makeText(VisitorModeActivity.this, "协议2", Toast.LENGTH_SHORT).show();
                }
            }
        }, fileName1, fileName2);
    }

    @Override
    public void onClick(View v) {
        Toast.makeText(this, "弹窗", Toast.LENGTH_SHORT).show();
    }
}
