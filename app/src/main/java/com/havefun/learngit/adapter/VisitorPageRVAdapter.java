package com.havefun.learngit.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.havefun.learngit.R;
import com.havefun.learngit.model.VisitorDataBean;

import java.util.ArrayList;
import java.util.List;

public class VisitorPageRVAdapter extends RecyclerView.Adapter<VisitorPageRVAdapter.ViewHolder> {
    private List<VisitorDataBean.RecommendJobsBean> mDataList = new ArrayList<>(16);
    private Context mCtx;
    private LayoutInflater mInflater;

    public VisitorPageRVAdapter(Context context) {
        mCtx = context;
        mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = mInflater.inflate(R.layout.app_recycle_item_visitor_layout, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        VisitorDataBean.RecommendJobsBean mockBean = mDataList.get(position);
        holder.tvJobTitle.setText(mockBean.getTitle());
        holder.tvSalary.setText(mockBean.getSalary());
        holder.tvCompany.setText(mockBean.getCompany());
        holder.tvStatus.setText(mockBean.getCompStage());
        holder.tvEmployeeNum.setText(mockBean.getEcompDetail().getEcompScaleName());
        holder.tvWorkYears.setText(mockBean.getRequireWorkYears());
        holder.tvEduInfo.setText(mockBean.getRequireEduLevel());
        holder.tvSpecialty.setText(mockBean.getEcompDetail().getEcompIndustrysName());
        holder.tvHRInfo.setText(safeParseArray(mockBean.getUserTags()));
        holder.tvLiveIn.setText(mockBean.getDq());
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    private String safeParseArray(List<String> userTags) {
        if (userTags == null || userTags.isEmpty()) {
            return "";
        }
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < userTags.size(); i++) {
            if (i > 0 && i == userTags.size() - 1) {
                sb.append("ㆍ");
            }
            sb.append(userTags.get(i));
        }
        return sb.toString();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvJobTitle;
        TextView tvSalary;
        TextView tvCompany;
        TextView tvStatus;
        TextView tvEmployeeNum;
        TextView tvWorkYears;
        TextView tvEduInfo;
        TextView tvSpecialty;
        TextView tvLiveIn;
        TextView tvHRInfo;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            TextView tvJobTitle = itemView.findViewById(R.id.tvJobTitle);
            TextView tvSalary = itemView.findViewById(R.id.tvSalary);
            TextView tvCompany = itemView.findViewById(R.id.tvCompany);
            TextView tvStatus = itemView.findViewById(R.id.tvStatus);
            TextView tvEmployeeNum = itemView.findViewById(R.id.tvEmployeeNum);
            TextView tvWorkAge = itemView.findViewById(R.id.tvWorkYears);
            TextView tvEduInfo = itemView.findViewById(R.id.tvEduInfo);
            TextView tvSpecialty = itemView.findViewById(R.id.tvSpecialty);
            TextView tvLiveIn = itemView.findViewById(R.id.tvLiveIn);
            TextView tvHRInfo = itemView.findViewById(R.id.tvHRInfo);
        }
    }
}
