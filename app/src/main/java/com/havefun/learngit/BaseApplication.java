package com.havefun.learngit;

import android.app.Application;

public class BaseApplication extends Application {
    private static BaseApplication baseApplication;
    public static Application getGlobalContext() {
        return baseApplication;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        baseApplication = this;
    }
}
