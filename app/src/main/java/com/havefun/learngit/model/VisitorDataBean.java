package com.havefun.learngit.model;

import java.util.List;

public class VisitorDataBean {

    private List<RecommendJobsBean> recommendJobs;

    public List<RecommendJobsBean> getRecommendJobs() {
        return recommendJobs;
    }

    public void setRecommendJobs(List<RecommendJobsBean> recommendJobs) {
        this.recommendJobs = recommendJobs;
    }

    public static class RecommendJobsBean {
        /**
         * ecUserId : f63d46f6b169a74212714f879a2c6fda
         * job_kind : 2
         * title : 安全测试工程师
         * url : 6034ba5a8365db33e08f772a04u.jpeg
         * company : 阿里巴巴(中国)软件有限公司
         * ecompStage : A轮
         * compStage : A轮
         * job_id : 2048951
         * salary : 10-15k·17薪
         * imFlag : 0
         * dq : 北京-朝外
         * isTopJob : 0
         * userTags : ["郭先生","产品经理"]
         * isApply : 0
         * date : 一个月前
         * publisherOnline : 0
         * requireWorkYears : 3-5年
         * jobCardTags : ["北京-朝外","3-5年","博士"]
         * ecompDetail : {"ecompIndustrysName":"互联网/电商","ecompScaleName":"10000人以上","ecompKindName":"A轮","name":"阿里巴巴(中国)软件有限公司"}
         * photo : 5ff7c994f959e32ac935b7a904u.png
         * requireEduLevel : 博士
         * honestHunter : 0
         * subwayStation : 朝阳门
         */

        private String ecUserId;
        private int job_kind;
        private String title;
        private String url;
        private String company;
        private String ecompStage;
        private String compStage;
        private int job_id;
        private String salary;
        private int imFlag;
        private String dq;
        private int isTopJob;
        private int isApply;
        private String date;
        private int publisherOnline;
        private String requireWorkYears;
        private EcompDetailBean ecompDetail;
        private String photo;
        private String requireEduLevel;
        private String honestHunter;
        private String subwayStation;
        private List<String> userTags;
        private List<String> jobCardTags;

        public String getEcUserId() {
            return ecUserId;
        }

        public void setEcUserId(String ecUserId) {
            this.ecUserId = ecUserId;
        }

        public int getJob_kind() {
            return job_kind;
        }

        public void setJob_kind(int job_kind) {
            this.job_kind = job_kind;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }

        public String getEcompStage() {
            return ecompStage;
        }

        public void setEcompStage(String ecompStage) {
            this.ecompStage = ecompStage;
        }

        public String getCompStage() {
            return compStage;
        }

        public void setCompStage(String compStage) {
            this.compStage = compStage;
        }

        public int getJob_id() {
            return job_id;
        }

        public void setJob_id(int job_id) {
            this.job_id = job_id;
        }

        public String getSalary() {
            return salary;
        }

        public void setSalary(String salary) {
            this.salary = salary;
        }

        public int getImFlag() {
            return imFlag;
        }

        public void setImFlag(int imFlag) {
            this.imFlag = imFlag;
        }

        public String getDq() {
            return dq;
        }

        public void setDq(String dq) {
            this.dq = dq;
        }

        public int getIsTopJob() {
            return isTopJob;
        }

        public void setIsTopJob(int isTopJob) {
            this.isTopJob = isTopJob;
        }

        public int getIsApply() {
            return isApply;
        }

        public void setIsApply(int isApply) {
            this.isApply = isApply;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public int getPublisherOnline() {
            return publisherOnline;
        }

        public void setPublisherOnline(int publisherOnline) {
            this.publisherOnline = publisherOnline;
        }

        public String getRequireWorkYears() {
            return requireWorkYears;
        }

        public void setRequireWorkYears(String requireWorkYears) {
            this.requireWorkYears = requireWorkYears;
        }

        public EcompDetailBean getEcompDetail() {
            return ecompDetail;
        }

        public void setEcompDetail(EcompDetailBean ecompDetail) {
            this.ecompDetail = ecompDetail;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getRequireEduLevel() {
            return requireEduLevel;
        }

        public void setRequireEduLevel(String requireEduLevel) {
            this.requireEduLevel = requireEduLevel;
        }

        public String getHonestHunter() {
            return honestHunter;
        }

        public void setHonestHunter(String honestHunter) {
            this.honestHunter = honestHunter;
        }

        public String getSubwayStation() {
            return subwayStation;
        }

        public void setSubwayStation(String subwayStation) {
            this.subwayStation = subwayStation;
        }

        public List<String> getUserTags() {
            return userTags;
        }

        public void setUserTags(List<String> userTags) {
            this.userTags = userTags;
        }

        public List<String> getJobCardTags() {
            return jobCardTags;
        }

        public void setJobCardTags(List<String> jobCardTags) {
            this.jobCardTags = jobCardTags;
        }

        public static class EcompDetailBean {
            /**
             * ecompIndustrysName : 互联网/电商
             * ecompScaleName : 10000人以上
             * ecompKindName : A轮
             * name : 阿里巴巴(中国)软件有限公司
             */

            private String ecompIndustrysName;
            private String ecompScaleName;
            private String ecompKindName;
            private String name;

            public String getEcompIndustrysName() {
                return ecompIndustrysName;
            }

            public void setEcompIndustrysName(String ecompIndustrysName) {
                this.ecompIndustrysName = ecompIndustrysName;
            }

            public String getEcompScaleName() {
                return ecompScaleName;
            }

            public void setEcompScaleName(String ecompScaleName) {
                this.ecompScaleName = ecompScaleName;
            }

            public String getEcompKindName() {
                return ecompKindName;
            }

            public void setEcompKindName(String ecompKindName) {
                this.ecompKindName = ecompKindName;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }
}
