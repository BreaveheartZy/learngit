package com.havefun.learngit;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.BackgroundColorSpan;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class BraveHeartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_winter_sweett);

        TextView textView = findViewById(R.id.textView);
        textView.setHighlightColor(Color.WHITE);
        String prefix = "我已阅读并同意";
        String protocol = "《猎聘用户服务协议》 《个人信息保护\n政策》";
        SpannableStringBuilder stringBuilder = new SpannableStringBuilder();
        stringBuilder.append(prefix);
        stringBuilder.append(protocol);
        stringBuilder.append("，未注册的手机号将自动完成账号注册");
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                Toast.makeText(BraveHeartActivity.this, "点击了用户协议", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                ds.setColor(Color.RED);
                ds.setUnderlineText(false);
            }
        };
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        stringBuilder.setSpan(clickableSpan,prefix.length(),prefix.length() + protocol.length(),
                Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        textView.setText(stringBuilder);
    }
}